var capteurContainer = document.getElementById('capteur-container') //Sélectionne le block (<section> HTML) contenant les cards (<article>) des catpeurs

var alertHTML = document.getElementById('alert-container') //Selectionne le conteneur de l'alerte critique
var alertValueHTML = document.querySelector('#data-temp-alert strong') //Selectionne le conteneur de la valeur de la température
var alertMsgHTML = document.getElementById('alert-msg') //Selectionne lle contenueur du msg d'alerte
var alertIndicateurHTML = document.getElementById('alert-indicateur-temp'); //Sélectionne l'inciateur de température (inférieure ou supérieure)


//Objet Capteur avec constructeur pour générer le code html de la card.
//Pemet d'ajouter des capteurs dynamiquement à la page.
var numInstance = 0 //Contient le numéro de l'instance de l'objet Capteur => pour différencier les instances

const EXTERIEUR = 'exterieur'
const INTERIEUR = 'interieur'
let Capteur = function(name, loc) {
    this.name = name;
    this.loc = loc;
    this.valueTemp = 0;
    this.msg = ''
    this.alertLogo = ''
    this.color = ''
    this.numInstance = numInstance
    this.historyValues = [] //Pour les 5 dernières valeurs de températures [historique]
    this.historyHours = [] //Pour les 5 dernières heures [historique]


    numInstance++
    capteurContainer.innerHTML +=
        '<article class="capteur" id="capteur-' + this.numInstance + '" >' +
        '<header class="capteur-header">' +
        '<h3>' + name + '</h3>' +
        '<a class="link-edit-capteur" href="#">Editer</a>' +
        '</header>' +
        '<section class="capteur-main">' +
        '<h4>Température</h4>' +
        '<data class="data-temp" id="data-temp-' + this.numInstance + '" value="20"><strong>20</strong>' +
        '<img class="img-alert" src="/assets/img/alert.png"/>' +
        '</data>' +
        '<p>Température idéale</p>' +
        '</section>' +
        '<section class="capteur-aside">' +
        '<h4>Historique</h4>' +
        '<div class="historique" id="historique-' + this.numInstance + '">' +
        '<ul class="liste-heure">' +
        '<li class="heure-item last-item" id="heure-' + this.numInstance + '-item-5"></li>' +
        '<li class="heure-item" id="heure-' + this.numInstance + '-item-4"></li>' +
        '<li class="heure-item" id="heure-' + this.numInstance + '-item-3"></li>' +
        '<li class="heure-item" id="heure-' + this.numInstance + '-item-2"></li>' +
        '<li class="heure-item" id="heure-' + this.numInstance + '-item-1"></li>' +
        '</ul>' +
        '<ul class="historique-liste-value">' +
        '<li class="historique-value-item last-item" id="historique-' + this.numInstance + '-value-item-5">?</li>' +
        '<li class="historique-value-item" id="historique-' + this.numInstance + '-value-item-4">?</li>' +
        '<li class="historique-value-item" id="historique-' + this.numInstance + '-value-item-3">?</li>' +
        '<li class="historique-value-item" id="historique-' + this.numInstance + '-value-item-2">?</li>' +
        '<li class="historique-value-item" id="historique-' + this.numInstance + '-value-item-1">?</li>' +
        '</ul>' +
        '</div>' +
        '<a href="#">Voir plus</a>' +
        '</section>' +
        '</article>'
}

//méthode permettant de définir les alertes en fonction de la température et de la localisation du capteur (intérieur ou extérieur)
Capteur.prototype.setAlert = function(value) {
    this.valueTemp = value //On met à jour la valeur de la température en interne (pour l'instance)

    var classes, indicateur;

    if (this.loc == INTERIEUR) {
        if (this.valueTemp >= 34) {
            classes = 'd-block'
            this.msg = 'Appelez les pompiers ou arrêtez votre barbecue !'
            indicateur = 'supérieure'
            this.color = 'alert-red'
            this.alertLogo = 'd-inline'
        } else if (this.valueTemp <= 12 && this.valueTemp > 0) {
            classes = 'd-block'
            this.msg = 'Montez le chauffage ou mettez un gros pull !'
            indicateur = 'inférieure'
            this.color = 'alert-blue'
            this.alertLogo = 'd-inline'
        } else if (this.valueTemp <= 0) {
            classes = 'd-block'
            indicateur = 'inférieure'
            this.msg = 'Canalisations gelées, appelez SOS plombier et mettez un bonnet !'
            this.color = 'alert-blue-cold'
            this.alertLogo = 'd-inline'
        } else if (this.valueTemp >= 22 && value < 26) {
            if (classes != 'd-block') {
                classes = 'd-none'
            }
            this.msg = 'Baissez le chauffage !'
            indicateur = 'supérieur'
            this.color = 'alert-yellow'
            this.alertLogo = 'd-none'
        } else if (this.valueTemp >= 26 && value < 34) {
            if (classes != 'd-block') {
                classes = 'd-none'
            }
            this.msg = 'Baissez le chauffage !'
            indicateur = 'supérieur'
            this.color = 'alert-orange'
            this.alertLogo = 'd-inline'
        } else {
            if (classes != 'd-block') {
                classes = 'd-none'
            } //if
            this.msg = 'Témpérature idéale'
            this.color = 'alert-green'
            this.alertLogo = 'd-none'
        } //else
    } //if

    if (this.loc == EXTERIEUR) {
        if (this.valueTemp >= 35) {
            classes = 'd-block'
            this.msg = 'Hot Hot Hot ! '
            indicateur = 'supérieure'
            this.color = 'alert-red'
            this.alertLogo = 'd-inline'

        } else if (this.valueTemp <= 14) {
            classes = 'd-block'
            this.msg = 'Banquise en vue !'
            indicateur = 'inférieure'
            this.color = 'alert-blue-cold'
            this.alertLogo = 'd-inline'
        } else {
            classes = 'd-none'
            this.msg = 'Témpérature idéale'
            this.color = 'alert-green'
            this.alertLogo = 'd-none'
        }
    }


    //Affichage graphique de l'erreur
    alertHTML.className = classes + ' alert-container'
    alertValueHTML.innerHTML = value
    alertValueHTML.value = value
    alertMsgHTML.innerHTML = this.msg
    alertIndicateurHTML.innerHTML = indicateur

    //Affichage du msg dans la carde du capteur (<article>)
    var capteurMsgHTML = document.querySelector('#capteur-' + this.numInstance + ' p')
    capteurMsgHTML.innerHTML = this.msg;
    capteurMsgHTML.className = this.color
    console.log('#capteur-' + this.numInstance + ' p', capteurMsgHTML, this.msg)

    //Gestion du logo de danger
    var capteurLogoAlertHTML = document.querySelector('#capteur-' + this.numInstance + ' .img-alert')
    capteurLogoAlertHTML.className = this.alertLogo + ' img-alert';

}

/*
 * Fonction permettant d'actualiser la température sur les cards des différents capteur
 */
Capteur.prototype.setTemp = function (value) {
    var tempHTML = document.querySelector('#capteur-' + this.numInstance + ' #data-temp-' + this.numInstance + ' strong')
    console.log('selector : ', tempHTML, value)
    tempHTML.innerHTML = value
} //end function

const TEMP_MAX = 36 //Permet de définir pour quelle valeur max de la température, l'affichage graphique des colones de l'historique sera au max de sa hauteur
const TEMP_MIN = 0 //Permet de définir pour quelle valeur min de la température, l'affichage graphique des colones de l'historique sera au minimum de sa hauteur

/*
 * Fonction permettant de gérer graphiquement l'historique
 */
Capteur.prototype.setHistory = function (value) {
    //On répète 5 fois pour les 5 colones de l'historique
    for (var i = 1; i < 6; i++) {
       var historyValueHTML = document.getElementById('historique-' + this.numInstance + '-value-item-' + i)
       var historyHourHTML = document.getElementById('heure-' + this.numInstance + '-item-' + i)

       if (this.historyValues[i - 1] != undefined) {
           //On affiche la valeur numérique de la température
           historyValueHTML.innerHTML = this.historyValues[i - 1]

           //On actualise le temps
           historyHourHTML.innerHTML = this.historyHours[i - 1]


           /** Gestion de la couleur des colones **/
           var color = 'black'
           if (this.historyValues[i - 1] < 12) {
               color = 'alert-blue-cold'
           } else if (this.historyValues[i - 1] <= 18) {
               color = 'alert-blue'
           } else if (this.historyValues[i - 1] <= 22) {
               color = 'alert-green'
           } else if (this.historyValues[i - 1] <= 26) {
               color = 'alert-yellow'
           } else if (this.historyValues[i - 1] <= 34) {
               color = 'alert-orange'
           } else if (this.historyValues[i - 1] > 34) {
               color = 'alert-red'
           }
           historyValueHTML.className = color + ' historique-value-item' + (i == 5 ? ' last-item' : '')

           //On définit en pourcentage la taille d'une colone de température à partir de la valeur de la température
           historyValueHTML.style.height = (Slider(this.historyValues[i - 1]) > 100 ? 100 : Slider(this.historyValues[i - 1])) + "%";


       } else {
           //A l'initialisation du capteur, s'il n'y a pas d'hitorique on remplace "undefined" par "?"
           historyValueHTML.innerHTML = '?'
        } //else
    } //for

    var date = new Date()
    //Ajout de la dernière valeur de la température au début du tableau
    this.historyValues.unshift(value)
    //Ajout des dernière 10 seconde (pour test, destiné à être uniquement l'heure)
    this.historyHours.unshift(date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()))

    //Si on a plus de 5 valeur de température, on retire la valeur la plus ancienne du tableau
    if(this.historyValues.length >= 5 && historyHourHTML >= 5) {
        this.historyValues.splice(5, 1);
        this.historyHours.splice(5, 1);
    } //if


} //end function

//Fonction permettant de convertir la temperature donnée en pourcentage sur une plage de température donnée
function Slider(pos) {
    var minM = TEMP_MIN;
    var maxM = TEMP_MAX;
    var minV = Math.log(15); //L'affichage des colones de température de l'historique sera au minimum de 15% de hauteur
    var maxV = Math.log(100); //L'affichage des colones de température sera de 100% de hauteur maximum
    var scal = (maxV - minV) / (maxM - minM);
    return parseInt(Math.exp(minV + scal * (pos - minM)));
}