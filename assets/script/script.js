var capteurInt = new Capteur('Capteur Intérieur', INTERIEUR)
var capteurExt = new Capteur('Capteur Extérieur', EXTERIEUR)

const TEMPS_ACTUALISATION = 10000 //Temps d'actualisation des capteur (en ms)

fetch("value.json", {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then(response => response.json()).then(function(data) {

        var tempInt = data.temperature.int.value
        var tempExt = data.temperature.ext.value

        var i = 0;
        //Cette fonction est appelée toutes les 10 secondes pour simuler les différentes valeurs de températures
        var simulation = function () {
            capteurExt.setTemp(tempExt[i])
            capteurExt.setAlert(tempExt[i])

            capteurInt.setTemp(tempInt[i])
            capteurInt.setAlert(tempInt[i])

            //Mise à jour de l'historique
            //Sur une application réel, l'historique serait mit à jour toutes les heures
            //10 secondes ici (via TEMPS_ACTUALISATION) pour la demo
            capteurExt.setHistory(tempExt[i])
            capteurInt.setHistory(tempInt[i])
        }
        simulation() //Initialisation

        //On appelle la fonction toutes les 10 secondes pour parcourir toutes les valeurs JSON
        // et on revient au début après la dernière valeur
        setInterval(function () {
            //Pour parcourir les valeurs JSON
            i++;
            if (i == tempInt.length) {
                i = 0
            }

            simulation()
        }, TEMPS_ACTUALISATION)

    })
    .catch(error => alert("Erreur : " + error));
