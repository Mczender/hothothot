<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/style/style.css">
    <title>HotHotHot</title>
</head>
<body>
    <header class="header">
        <a href="./index.php"><h1 class="h1">HotHotHot</h1></a>
        <nav class="nav-container"> 
            <ul class="nav-container-items">
                <li id="nav-item-3"><a href="./parametres.html" class="a-nav"><p class="link-label">Paramètres</p><img src="./assets/img/params.svg" alt=""></a></li>
                <li id="nav-item-1"><a href="#" class="a-nav"><p class="link-label">Mon Compte</p><img src="./assets/img/user.svg" alt=""></a></li>
                <li id="nav-item-2"><a href="#" class="a-nav"><p class="link-label">Alertes </p><data class="number-alert" value="0">0</data></a></li>
            </ul>
            <div class="trait"></div>
        </nav>
        <p class="time">13:50</p>
    </header>

    <main class="main">
        <section id="alert-container" class="alert-container">
            <h2 class="alert-title">Alertes</h2>
            <p class="alert-header">La température est <strong id="alert-indicateur-temp">inférieur</strong> à <data class="data-temp" id="data-temp-alert" value="12"><strong>12</strong></data></p>
            <p id="alert-msg" class="alert-content">Montez le chauffage !! Et mettez un gros pull !!</p>
        </section>
        <section class="capteur-container" id="capteur-container">
            <h2 class="h2">Informations capteurs</h2>
            <!-- Code généré en JS

            <article class="capteur" id="capteur-' + this.numInstance + '" >
                <h4>Historique</h4>
                <header class="capteur-header">
                    <h3>Capteur intérieur</h3>
                    <a class="link-edit-capteur" href="#">Editer</a>
                    </header>
                <section class="capteur-main">
                    <h4>Température</h4>
                    <data class="data-temp" id="data-temp" value="20"><strong>20</strong></data>
                    <p>Température idéale</p>
                    </section>
                <section class="capteur-aside">
                    <h4>Historique</h4>
                    <article id="historique-1" class="historique" >
                        <ul class="liste-heure">
                            <li class="heure-item last-item" id="heure-0-item-5"></li>
                            <li class="heure-item" id="heure-0-item-4"></li>
                            <li class="heure-item" id="heure-0-item-3"></li>
                            <li class="heure-item" id="heure-item-2"></li>
                            <li class="heure-item" id="heure-0-item-1"></li>
                        </ul>
                        <ul class="historique-liste-value">
                            <li class="historique-value-item last-item" id="historique-0-value-item-5">?</li>
                            <li class="historique-value-item" id="historique-0-value-item-4">?</li>
                            <li class="historique-value-item" id="historique-0-value-item-3">?</li>
                            <li class="historique-value-item" id="historique-0-value-item-2">?</li>
                            <li class="historique-value-item" id="historique-0-value-item-1">?</li>
                        </ul>
                    </article>
                    <a href="#">Voir plus</a>
                </section>
            </article>
                -->
        </section>
    </main>

    <script src="assets/script/capteur.js"></script>
    <script src="assets/script/script.js"></script>
</body>
</html>