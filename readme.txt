BUFFARD Gabriel

Projet HotHotHot

Partie HTML : 

Au début du cours j’étais plutôt à l’aise avec le HTML mais j’avais l’habitude d’utiliser principalement et abusivement les 
balises <div></div> pour structurer mes différents blocks, tout en ayant connaissance des différentes balises sémantique de HTML5. 
Au cours de ce projet, j’ai réussi à ne presque pas utiliser de <div>. 

Mes blocks sont organisés en <section></section>, sauf l’entête de la page (titre – menu) qui est contenu dans un <header>. 
Le contenu principal de la page est dans une balise <main></main>. 

Il y a ensuite 2 <section> principales : une pour les alertes, et une pour le conteneur des différents capteurs qui sont eux-mêmes contenus 
dans des <article></article>.

J’ai veillé à faire une page valide par le standard W3C durant tout le développement. 



Partie CSS : 
Avant ce cours j’avais fait assez peu de CSS natif (j’en ai fait un certain temp, mais sans être très bon, surtout pour l’organisation des règles) 
et beaucoup de bootstrap depuis un moment. 

J’ai donc choisi d’explorer la voix des préprocesseurs CSS souhaitant progresser sur le langage CSS en lui-même. 
J’en avais entendu parler mais je n’avais jamais eu l’occasion de pratiquer. Mon choix c’est orienté sur SASS qui je pense est le plus connu. 

J’ai donc morcelé mon code autant que possible, chaque partie de la page étant gérée dans un fichier différent. J’ai beaucoup utilisé 
le principe d’imbrication des sélecteurs et j’ai utilisé les variables pour les différentes couleurs présentes sur le site.

La grande difficulté ici à été de gérer l’aspect responsive du site. De plus, en CSS je maitrisais déjà assez bien « Flexbox » 
que ce soit en natif ou avec bootstrap. J’ai donc choisi de m’initier aux « grid » que je n’avais jamais pratiqué auparavant. 
J’ai rencontré pas mal de difficultés lors de l’apprentissage, mais j’ai fini par comprendre le principe qui avec du recul est plutôt simple.

D’un point de vue design, j’ai choisi de jouer sur les couleurs pour montrer le degré de l’alerte. 
Le problème est que pour les personnes mal-voyantes ou daltoniennes, les couleurs peuvent ne pas suffire. J’ai donc ajouté un pictogramme de danger à côté de l’affichage de la température du capteur en question qui s’affiche uniquement quand l’alerte devient critique.
Les alertes les plus dangereuses s’affichent en gros et rouge en haut de l’écran.



Partie Javascript : 

Avant ce cours, j’avais l’habitude de charger la librairie JQuery. J’ai donc fait du javascript purement natif. 

Pour aller récupérer les valeurs définies dans le fichier JSON, j’ai utilisé Fetch que j’ai découvert avec ce projet. 
Avant j’utiliser principalement XMLHttpRequest pour faire mes requêtes Ajax. J’ai trouvé l’utilisation de Fetch plus souple et plus puissante. 

J’ai également choisi de générer le code HTML qui permet de gérer l’affichage des « cards » des capteurs en javascript. 
Le but est d’imaginé pouvoir ajouter dynamiquement des capteurs depuis la page « paramètres » du site lorsque celui-ci fonctionnera avec une partie serveur. 
Pour faire cela j’ai créé un objet javascript avec un constructeur qui génère le code HTML. Pour l’instant, je crée des instances de mes 
capteur dans le script principal manuellement.

De plus, l’actualisation de la température se fait toutes les 10 secondes. On pourrait peut-être l’augmenter pour une réelle application. 
L’historique (que j’ai fait à 100% mise à part la fonction « Slider() » permettant de mapper ma plage de valeur minimale et maximale en pourcentage pour gérer 
la hauteur de mes colonnes) sur l’application réel doit être dans l’idéal mit à jour toutes les heures pour avoir un aperçu sur le long terme des dernières valeurs. 
Ici, il est actualisé toutes les 10 secondes pour la démo. 

J’ai essayé de penser mon code orienté objet le plus possible pour la gestion (graphique) des capteurs. Auparavant Je n’avais jamais fait d’objet en JS même si j’avais quelques notions.


Bilan personnel : 

J’ai mis du temps à réaliser cette application car j’ai essayé de faire attentions au plus de détails possibles. Préférant le développement Backend au Frontend, 
ce projet à était un petit défi personnel, notamment pour essayer de faire une accessibilité qui est correcte et des animations en 
javascript qui est un langage dont je n’ai pas énormément l’habitude par rapport au PHP.
De plus, j’ai le sentiment d’avoir bien progressé en CSS, le préprocesseur SASS a été une très bonne découverte, notamment pour sa capacité à 
morceler le code et ainsi avoir une meilleure organisation qui est pour moi indispensable.
